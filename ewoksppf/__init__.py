from .bindings import execute_graph  # noqa: F401

__version__ = "0.0.3-alpha.2"
