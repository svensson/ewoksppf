# EwoksPpf: Pypushflow binding for Ewoks

ewoksppf provides task scheduling for cyclic Ewoks workflows.

## Install

```bash
python -m pip install ewoksppf[test]
```

## Test

```bash
pytest --pyargs ewoksppf.tests
```

## Documentation

https://workflow.gitlab-pages.esrf.fr/ewoks/ewoksppf/
