ewoksppf |release|
==================

ewoksppf provides task scheduling for cyclic Ewoks workflows.

ewoksppf has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.eu/>`_.

.. toctree::
    :maxdepth: 2

    actors
    api
